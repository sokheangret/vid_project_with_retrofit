
package com.example.admin.myretrofit.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class User {

    @SerializedName("code")
    private String mCode;
    @SerializedName("data")
    private List<Datum> mData;
    @SerializedName("msg")
    private String mMsg;
    @SerializedName("status")
    private Boolean mStatus;

    public String getCode() {
        return mCode;
    }

    public void setCode(String code) {
        mCode = code;
    }

    public List<Datum> getData() {
        return mData;
    }

    public void setData(List<Datum> data) {
        mData = data;
    }

    public String getMsg() {
        return mMsg;
    }

    public void setMsg(String msg) {
        mMsg = msg;
    }

    public Boolean getStatus() {
        return mStatus;
    }

    public void setStatus(Boolean status) {
        mStatus = status;
    }

    @Override
    public String toString() {
        return "User{" +
                "mCode='" + mCode + '\'' +
                ", mData=" + mData +
                ", mMsg='" + mMsg + '\'' +
                ", mStatus=" + mStatus +
                '}';
    }
}
