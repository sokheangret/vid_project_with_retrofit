
package com.example.admin.myretrofit.model;


import com.google.gson.annotations.SerializedName;


public class SubCate {

    @SerializedName("cate_name")
    private String CateName;
    @SerializedName("des")
    private String Des;
    @SerializedName("icon_name")
    private Object IconName;
    @SerializedName("id")
    private Long Id;
    @SerializedName("status")
    private Boolean Status;
    @SerializedName("total_url")
    private Long TotalUrl;

    public String getCateName() {
        return CateName;
    }

    public void setCateName(String cateName) {
        CateName = cateName;
    }

    public String getDes() {
        return Des;
    }

    public void setDes(String des) {
        Des = des;
    }

    public Object getIconName() {
        return IconName;
    }

    public void setIconName(Object iconName) {
        IconName = iconName;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    public Long getTotalUrl() {
        return TotalUrl;
    }

    public void setTotalUrl(Long totalUrl) {
        TotalUrl = totalUrl;
    }

}
