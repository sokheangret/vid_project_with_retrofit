
package com.example.admin.myretrofit.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class MainCatagory {

    @SerializedName("code")
    private String Code;
    @SerializedName("data")
    private List<Datum> Data;
    @SerializedName("msg")
    private String Msg;
    @SerializedName("status")
    private Boolean Status;

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public List<Datum> getData() {
        return Data;
    }

    public void setData(List<Datum> data) {
        Data = data;
    }

    public String getMsg() {
        return Msg;
    }

    public void setMsg(String msg) {
        Msg = msg;
    }

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    @Override
    public String toString() {
        return "MainCatagory{" +
                "Code='" + Code + '\'' +
                ", Data=" + Data +
                ", Msg='" + Msg + '\'' +
                ", Status=" + Status +
                '}';
    }
}
