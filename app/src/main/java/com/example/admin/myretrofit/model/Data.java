
package com.example.admin.myretrofit.model;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class Data {

    @SerializedName("email")
    private String mEmail;
    @SerializedName("id")
    private Long mId;
    @SerializedName("password")
    private String mPassword;
    @SerializedName("role")
    private Role mRole;
    @SerializedName("status")
    private Boolean mStatus;
    @SerializedName("user_name")
    private String mUserName;

    public Data(String mEmail, Long mId, String mPassword, Role mRole, Boolean mStatus, String mUserName) {
        this.mEmail = mEmail;
        this.mId = mId;
        this.mPassword = mPassword;
        this.mRole = mRole;
        this.mStatus = mStatus;
        this.mUserName = mUserName;
    }


    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public Role getRole() {
        return mRole;
    }

    public void setRole(Role role) {
        mRole = role;
    }

    public Boolean getStatus() {
        return mStatus;
    }

    public void setStatus(Boolean status) {
        mStatus = status;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

}
