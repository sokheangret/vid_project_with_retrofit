package com.example.admin.myretrofit.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.myretrofit.ListAdapter;
import com.example.admin.myretrofit.R;
import com.example.admin.myretrofit.model.CreateUser;
import com.example.admin.myretrofit.model.Datum;
import com.example.admin.myretrofit.model.Role;
import com.example.admin.myretrofit.model.User;
import com.example.admin.myretrofit.repository.CategoryRepository;
import com.example.admin.myretrofit.service.ServiceGenerator;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UsersActivity extends AppCompatActivity {
    private Button btnshow;
    private TextView txtuser;
    private List<Datum> lstuser;
    private ListAdapter listAdapter;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        txtuser = (TextView) findViewById(R.id.mytext);
        btnshow = (Button) findViewById(R.id.btnuser);
        listView = (ListView) findViewById(R.id.userlist);

        lstuser = new ArrayList<>();
        listAdapter = new ListAdapter(this,lstuser);
        listView.setAdapter(listAdapter);

        btnshow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServiceGenerator.CreateService(CategoryRepository.class)
                                .getAllUsers()
                                .enqueue(new Callback<User>() {
                                    @Override
                                    public void onResponse(Call<User> call, Response<User> response) {
                                        if(response.isSuccessful()){
                                            lstuser.clear();
                                            lstuser.addAll(response.body().getData());
                                            //txtuser.setText(response.body().getData().toString());
                                            Log.e("Data",response.body().toString());
                                            Toast.makeText(UsersActivity.this, "success", Toast.LENGTH_SHORT).show();
                                            listAdapter.notifyDataSetChanged();
                                        }else {
                                            Toast.makeText(UsersActivity.this, "Fail", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<User> call, Throwable t) {
                                        Toast.makeText(UsersActivity.this, "Fail", Toast.LENGTH_SHORT).show();

                                    }
                                });
            }
        });


    }

    public void adduser(View view) {
        Role role = new Role();
        role.setId((long) 1);
        //Data data = new Data("hazard@gmail",123L,"a123",role ,true,"harzard");
        CreateUser createUser = new CreateUser("exvith@gmail.com","password123",0,1,"password123","extestingbyvid");
        createUser(createUser);
    }

    private void createUser(CreateUser data) {
        ServiceGenerator.CreateService(CategoryRepository.class)
                        .adduser(data)
                        .enqueue(new Callback<CreateUser>() {
                            @Override
                            public void onResponse(Call<CreateUser> call, Response<CreateUser> response) {
                                if (response.isSuccessful()){
                                    Toast.makeText(UsersActivity.this, "success add user", Toast.LENGTH_SHORT).show();
                                }else {
                                    Log.e("AAAAAA",response.message());
                                    Toast.makeText(UsersActivity.this, "Fail add user", Toast.LENGTH_SHORT).show();

                                }

                            }

                            @Override
                            public void onFailure(Call<CreateUser> call, Throwable t) {

                            }
                        });
    }


}
