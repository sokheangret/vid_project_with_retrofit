package com.example.admin.myretrofit.service;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by admin on 11/29/17.
 */

public class ServiceGenerator {

    private static final String BASE_URL = "http://110.74.194.125:15000/api/v1/";

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.build();

    public static <S> S CreateService(Class<S> serviceclass){
        return retrofit.create(serviceclass);

    }
}
