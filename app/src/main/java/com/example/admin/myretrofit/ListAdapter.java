package com.example.admin.myretrofit;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.admin.myretrofit.model.Datum;

import java.util.List;

/**
 * Created by admin on 12/3/17.
 */

public class ListAdapter extends BaseAdapter {
    Context context;
    List<Datum> datumList;
    ViewHolder viewHolder;

    public ListAdapter(Context context, List<Datum> datumList) {
        this.context = context;
        this.datumList = datumList;
    }

    @Override
    public int getCount() {
        return datumList.size();
    }

    @Override
    public Object getItem(int position) {
        return datumList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view==null){
            view = ((Activity)context).getLayoutInflater().inflate(R.layout.list_user,parent,false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        }else {
            viewHolder=(ViewHolder)view.getTag();
        }

        viewHolder.txtid.setText(datumList.get(position).getId().toString());
        viewHolder.txtname.setText(datumList.get(position).getUserName());
        viewHolder.txtemail.setText(datumList.get(position).getEmail());
        viewHolder.txtrole.setText(datumList.get(position).getRole().toString());

        return view;
    }

    class ViewHolder{
        TextView txtid,txtname,txtemail,txtrole;

        public ViewHolder(View v){
            txtid = v.findViewById(R.id.userid);
            txtname = v.findViewById(R.id.username);
            txtemail = v.findViewById(R.id.useremail);
            txtrole = v.findViewById(R.id.userrole);
        }
    }
}
