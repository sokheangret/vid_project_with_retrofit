package com.example.admin.myretrofit.repository;

import com.example.admin.myretrofit.model.CreateUser;
import com.example.admin.myretrofit.model.MainCatagory;
import com.example.admin.myretrofit.model.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by admin on 11/29/17.
 */

public interface CategoryRepository {

    @GET("categories")
    Call<MainCatagory> getAllData();

    @GET("users")
    Call<User> getAllUsers();

    @POST("users/create")
    Call<CreateUser> adduser(@Body CreateUser createUser);
}
