package com.example.admin.myretrofit.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sokheangret on 12/9/17.
 */

public class CreateUser {

    @SerializedName("email")
    private String email;
    @SerializedName("first_password")
    private String first_password;
    @SerializedName("id")
    private int id;
    @SerializedName("role_id")
    private int role_id;
    @SerializedName("second_password")
    private String second_password;
    @SerializedName("user_name")
    private String user_name;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirst_password() {
        return first_password;
    }

    public void setFirst_password(String first_password) {
        this.first_password = first_password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public String getSecond_password() {
        return second_password;
    }

    public void setSecond_password(String second_password) {
        this.second_password = second_password;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public CreateUser(String email, String first_password, int id, int role_id, String second_password, String user_name) {
        this.email = email;
        this.first_password = first_password;
        this.id = id;
        this.role_id = role_id;
        this.second_password = second_password;
        this.user_name = user_name;
    }
}
