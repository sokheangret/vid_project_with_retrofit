package com.example.admin.myretrofit.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.myretrofit.R;
import com.example.admin.myretrofit.model.MainCatagory;
import com.example.admin.myretrofit.repository.CategoryRepository;
import com.example.admin.myretrofit.service.ServiceGenerator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    Button button;
    TextView txtview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button) findViewById(R.id.btnshow);
        txtview = (TextView) findViewById(R.id.txtshow);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showdata();
            }
        });
    }

    public void showdata(){
        ServiceGenerator.CreateService(CategoryRepository.class)
                        .getAllData()
                        .enqueue(new Callback<MainCatagory>() {
                            @Override
                            public void onResponse(Call<MainCatagory> call, Response<MainCatagory> response) {
                                if(response.isSuccessful()){
                                    Log.e("date",response.body().toString());
                                    txtview.setText(response.body().toString());
                                    Toast.makeText(MainActivity.this, "Success", Toast.LENGTH_SHORT).show();
                                }else {
                                    Toast.makeText(MainActivity.this, "Fail", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(Call<MainCatagory> call, Throwable t) {
                                t.printStackTrace();
                            }
                        });
    }
}
