
package com.example.admin.myretrofit.model;


import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("email")
    private String mEmail;
    @SerializedName("id")
    private Long mId;
    @SerializedName("password")
    private Object mPassword;
    @SerializedName("role")
    private Role mRole;
    @SerializedName("status")
    private Boolean mStatus;
    @SerializedName("user_name")
    private String mUserName;

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Object getPassword() {
        return mPassword;
    }

    public void setPassword(Object password) {
        mPassword = password;
    }

    public Role getRole() {
        return mRole;
    }

    public void setRole(Role role) {
        mRole = role;
    }

    public Boolean getStatus() {
        return mStatus;
    }

    public void setStatus(Boolean status) {
        mStatus = status;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    @Override
    public String toString() {
        return "Datum{" +
                "mEmail='" + mEmail + '\'' +
                ", mId=" + mId +
                ", mPassword=" + mPassword +
                ", mRole=" + mRole +
                ", mStatus=" + mStatus +
                ", mUserName='" + mUserName + '\'' +
                '}';
    }
}
